package com.example.wheretobuy.ui.home;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wheretobuy.MainActivity;
import com.example.wheretobuy.R;
import com.example.wheretobuy.Retrofit.CategoryModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeViewHolder> {

    private List<CategoryModel> categories;
    public CategoryItemClickListener categoryItemClickListener;



    public HomeAdapter( List<CategoryModel> categories) {
        this.categories = categories;
    }

    public class HomeViewHolder extends RecyclerView.ViewHolder {

        public TextView categoryText;
        public ImageView image;
        CategoryItemClickListener categoryItemClickListener;

        public HomeViewHolder(@NonNull View itemView,final CategoryItemClickListener categoryItemClickListener) {
            super(itemView);

            categoryText = (TextView) itemView.findViewById(R.id.categoryText);
            image = (ImageView) itemView.findViewById(R.id.categoryImage);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(categoryItemClickListener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION ){
                            categoryItemClickListener.onCategoryItemClick(position);
                        }
                    }
                }
            });
        }


    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View cardView = inflater.inflate(R.layout.category_card_view, parent, false);


        // Return a new holder instance
        HomeViewHolder viewHolder = new HomeViewHolder(cardView,categoryItemClickListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        CategoryModel category = categories.get(position);

        TextView categoryText = holder.categoryText;
        categoryText.setText(category.getTitle());
        ImageView imageView = holder.image;


        // ugly part but I don't know in this moment how to use variable as id in setImageResource
        if(category.getImage().equals("bicycle_image.jpg")){
            imageView.setImageResource(R.drawable.bicycle_image);
        } else if (category.getImage().equals("beers.jpg")){
            imageView.setImageResource(R.drawable.beers);
        } else if (category.getImage().equals("car.jpg")){
            imageView.setImageResource(R.drawable.car);
        } else if (category.getImage().equals("computer.jpg")){
            imageView.setImageResource(R.drawable.computer);
        } else if (category.getImage().equals("fish.jpg")){
            imageView.setImageResource(R.drawable.fish);
        } else if (category.getImage().equals("fruit_basket.jpg")){
            imageView.setImageResource(R.drawable.fruit_basket);
        } else if (category.getImage().equals("pasta.jpg")){
            imageView.setImageResource(R.drawable.pasta);
        } else if (category.getImage().equals("restaurant.jpg")){
            imageView.setImageResource(R.drawable.restaurant);
        } else if (category.getImage().equals("sport.jpg")){
            imageView.setImageResource(R.drawable.sport);
        } else if (category.getImage().equals("vegetables.jpg")){
            imageView.setImageResource(R.drawable.vegetables);
        }

    }



    @Override
    public int getItemCount() {
        return categories.size();
    }

    void setClickListener(CategoryItemClickListener itemClickListener) {
        this.categoryItemClickListener = itemClickListener;
    }


    // parent activity will implement this method to respond to click events
    public interface CategoryItemClickListener {
        void onCategoryItemClick(int position);
    }
}
