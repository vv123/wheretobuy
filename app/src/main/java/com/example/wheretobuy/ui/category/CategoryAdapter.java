package com.example.wheretobuy.ui.category;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wheretobuy.R;
import com.example.wheretobuy.Retrofit.ProductModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ProductViewHolder> implements Filterable {

    private List<ProductModel> products;
    private List<ProductModel> allProducts;
    StorageReference storage = FirebaseStorage.getInstance().getReferenceFromUrl("gs://where-to-buy-293620.appspot.com/");
    FirebaseFirestore db = FirebaseFirestore.getInstance();



    public CategoryAdapter( List<ProductModel> products) {

        this.products = products;
        this.allProducts = new ArrayList<>(products);

    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ProductModel> filteredProducts = new ArrayList<>();

            if(constraint == null || constraint.length() == 0){
                filteredProducts.addAll(allProducts);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for(ProductModel productModel:allProducts){
                    if(productModel.getTitle().toLowerCase().contains(filterPattern)||
                           ( productModel.getBarcode() != null && productModel.getBarcode().toLowerCase().contains(filterPattern))){
                        filteredProducts.add(productModel);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filteredProducts;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            products.clear();
            if(results.values != null){
                products.addAll((List)results.values);
            }else{
                products.add(allProducts.get(0));
            }
            notifyDataSetChanged();
        }
    };


    public class ProductViewHolder extends RecyclerView.ViewHolder {

        public TextView productText;
        public ImageView image;
        public Button icon;


        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);

            productText = (TextView) itemView.findViewById(R.id.product_title);
            image = (ImageView) itemView.findViewById(R.id.product_image);
            icon = (Button) itemView.findViewById(R.id.favouriteIcon);

        }

    }

    @NonNull
    @Override
    public CategoryAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View cardView = inflater.inflate(R.layout.product_card_view, parent, false);


        // Return a new holder instance
        CategoryAdapter.ProductViewHolder viewHolder = new CategoryAdapter.ProductViewHolder(cardView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, final int position) {
        final ProductModel product = products.get(position);

        TextView categoryText = holder.productText;
        categoryText.setText(product.getTitle());
        final ImageView image = holder.image;

        StorageReference ref = storage.child(product.getImage());

        ref.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downUri = task.getResult();
                    String imageUrl = downUri.toString();
                    Picasso.get()
                            .load(imageUrl)
                            .into(image);
                }
            }
        });

        if(product.getIsFavorite()){
            holder.icon.setBackgroundResource(R.drawable.ic_baseline_favorite_24);

        } else {
            holder.icon.setBackgroundResource(R.drawable.ic_baseline_favorite_border_24);
        }

        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                product.setIsFavorite(!product.getIsFavorite());
                notifyDataSetChanged();
                db.collection("Beverage")
                        .document(product.getId())
                        .update("isFavourite",product.getIsFavorite());

                //if is favourite add it
                if(product.getIsFavorite()){
                    Map<String, Object> data = new HashMap<>();
                    data.put("id", product.getId());
                    data.put("name", product.getTitle());
                    db.collection("wishlist").document(product.getTitle()).set(data);
                } else {
                    db.collection("wishlist").document(product.getTitle()).delete();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return products.size();
    }


}
