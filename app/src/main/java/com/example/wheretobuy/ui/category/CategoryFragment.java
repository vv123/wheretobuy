package com.example.wheretobuy.ui.category;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wheretobuy.CaptureAct;
import com.example.wheretobuy.MainActivity;
import com.example.wheretobuy.R;
import com.example.wheretobuy.Retrofit.ProductModel;
import com.example.wheretobuy.Retrofit.StoreLocation;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;


public class CategoryFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<ProductModel> products = new ArrayList<ProductModel>();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final String TAG = "FAILED";
    LocationManager locationManager;
    private MainActivity activity;
    LocationListener locationListener;
    LatLng userLatLng = new LatLng(40.64427, -8.64554);
    private MarkerOptions options = new MarkerOptions();
    CategoryAdapter categoryAdapter;
    private FusedLocationProviderClient fusedLocationClient;

    private ArrayList<LatLng> latlngs = new ArrayList<>();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity);
        findLocation();

    }

    private OnMapReadyCallback callback = new OnMapReadyCallback() {


        @Override
        public void onMapReady(final GoogleMap googleMap) {
            for (LatLng point : latlngs) {
                options.position(point);
                googleMap.addMarker(options);
            }
            //I need to use current location with gps here if I have enough time
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 13.1f));

        }
    };


    @SuppressLint("MissingPermission")
    private void findLocation() {
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this.activity, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            userLatLng = new LatLng(location.getLatitude(),location.getLongitude());
                        }
                    }
                });

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View root = inflater.inflate(R.layout.fragment_category, container, false);
        recyclerView = root.findViewById(R.id.recycler_view_product);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        db.collection(activity.selectedCategoryModel.getTitle()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<ProductModel> newProducts = new ArrayList<ProductModel>();
                    for (DocumentSnapshot document : task.getResult()) {


                        Object title = document.get("name");
                        Object image = document.get("image");
                        Object isFavorite = document.get("isFavourite");
                        Object barcode = document.get("barcode");
                        Object id = document.getId();
                        HashMap<String,HashMap> stores  =(HashMap<String,HashMap>) document.get("stores");


                        if(stores != null){
                           List<StoreLocation> storeLocationList = new ArrayList<StoreLocation>();
                          for(Map.Entry<String,HashMap> store: stores.entrySet()){
                              HashMap<String,String> value = store.getValue();
                              StoreLocation storeLocation = new StoreLocation();
                              String lat = null;
                              String lang = null;
                              for(Map.Entry<String,String> latlang: value.entrySet()){

                                  if(latlang.getKey().equals("latitude")){
                                       lat = latlang.getValue();
                                  } else if (latlang.getKey().equals("longitude")){
                                      lang=latlang.getValue();
                                  }
                              }


                              if(lat != null && lang != null){
                                  storeLocation.setLongitude(lang);
                                  storeLocation.setLatitude(lat);

                              }
                              storeLocationList.add(storeLocation);
                          }

                            if(latlngs.isEmpty()){
                                for(int i=0;i<storeLocationList.size(); i++){
                                    latlngs.add(new LatLng(Double.parseDouble(storeLocationList.get(i).getLatitude())
                                            ,Double.parseDouble(storeLocationList.get(i).getLongitude())));
                                }


                            }

                        }
                        ProductModel pm = new ProductModel();


                        pm.setTitle(title.toString());
                        if(image != null){
                            pm.setImage(image.toString());
                        }
                        pm.setIsFavorite((Boolean) isFavorite);
                        pm.setId(id.toString());
                        if(barcode != null){
                            pm.setBarcode(barcode.toString());
                        }
                        newProducts.add(pm);
                    }

                    products = newProducts;

                    categoryAdapter = new CategoryAdapter(products);
                    recyclerView.setAdapter(categoryAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    categoryAdapter.notifyDataSetChanged();

                    SupportMapFragment mapFragment =
                            (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
                    if (mapFragment != null) {
                        mapFragment.getMapAsync(callback);
                    }



                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });




        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull final MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem itemSearch = menu.findItem(R.id.action_search);
        MenuItem itemBarcode = menu.findItem(R.id.barcode_search);

        SearchView searchViewString = (SearchView) itemSearch.getActionView();
        Button searchViewBarcode = (Button) itemBarcode.getActionView();

        searchViewBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scanCode();
            }
        });
        searchViewString.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                
                categoryAdapter.getFilter().filter(newText);
                return false;
            }
        });
    }

    public void scanCode() {

        IntentIntegrator intentIntegrator =  IntentIntegrator.forSupportFragment(CategoryFragment.this);
        intentIntegrator.setCaptureActivity(CaptureAct.class);
        intentIntegrator.setOrientationLocked(false);
        intentIntegrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        intentIntegrator.setPrompt("Scanning Code");
        intentIntegrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        IntentResult result =  IntentIntegrator.parseActivityResult(requestCode,requestCode,data);
        //use result here...in my case it is null
        categoryAdapter.getFilter().filter("5601164115606");
        if(result != null){
            if(result.getContents() != null){
                categoryAdapter.getFilter().filter(result.getContents());
            } else {
                //Toast.makeText(this.getContext(),"NO RESULT",Toast.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}