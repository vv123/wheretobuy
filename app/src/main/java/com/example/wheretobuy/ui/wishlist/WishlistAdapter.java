package com.example.wheretobuy.ui.wishlist;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wheretobuy.R;
import com.example.wheretobuy.Retrofit.CategoryModel;
import com.example.wheretobuy.Retrofit.ProductModel;
import com.example.wheretobuy.ui.home.HomeAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.WishlistViewHolder> {

    private List<ProductModel> products;
    FirebaseFirestore db = FirebaseFirestore.getInstance();


    public WishlistAdapter( List<ProductModel> products) {
        this.products = products;
    }

    @NonNull
    @Override
    public WishlistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View cardView = inflater.inflate(R.layout.wishlist_product_card_view, parent, false);

        // Return a new holder instance
        WishlistAdapter.WishlistViewHolder viewHolder = new WishlistAdapter.WishlistViewHolder(cardView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final WishlistViewHolder holder, final int position) {
        final ProductModel product = products.get(position);

        TextView productText = holder.productText;

        productText.setText(product.getTitle());

      /*  final ImageView image = holder.image;

        StorageReference ref = storage.child(product.getImage());

        ref.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downUri = task.getResult();
                     String imageUrl = downUri.toString();
                    Picasso.get()
                            .load(imageUrl)
                            .into(image);
                }else{
                }
            }
        });*/

        holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                products.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, products.size());
                notifyDataSetChanged();
                Log.d("id " , product.getId());
                db.collection("Beverage")
                        .document(product.getId())
                        .update("isFavourite",false);
                db.collection("wishlist").document(product.getTitle()).delete();

            }
        });
    }

    @Override
    public int getItemCount() {

        return products.size();
    }

    public class WishlistViewHolder extends RecyclerView.ViewHolder {

        public TextView productText;
        public ImageView image;
        public Button icon;


        public WishlistViewHolder(@NonNull View itemView) {
            super(itemView);

            productText = (TextView) itemView.findViewById(R.id.wishlist_product_title);
            //image = (ImageView) itemView.findViewById(R.id.wishlist_product_image);
            icon = (Button) itemView.findViewById(R.id.removeIcon);


        }
    }
}
