package com.example.wheretobuy.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.example.wheretobuy.MainActivity;
import com.example.wheretobuy.R;
import com.example.wheretobuy.Retrofit.CategoryModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class HomeFragment extends Fragment {

    private RecyclerView recyclerView;
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private List<CategoryModel> categories = new ArrayList<CategoryModel>();
    private final String TAG = "FAILED";
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private MainActivity activity;



    public HomeFragment(){

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        staggeredGridLayoutManager =  new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = rootView.findViewById(R.id.recycler_view);
        db.collection("catagories").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<CategoryModel> newCategories = new ArrayList<CategoryModel>();
                    int i = 0;
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Object image = document.get("image");
                        Object title = document.get("title");
                        CategoryModel cm = new CategoryModel();
                        cm.setImage(image.toString());
                        cm.setTitle(title.toString());
                        newCategories.add(cm);
                    }
                    Collections.sort(newCategories, new Comparator<CategoryModel>() {
                        @Override
                        public int compare(CategoryModel o1, CategoryModel o2) {
                            return o1.getTitle().compareTo(o2.getTitle());
                        }
                    });
                    categories = newCategories;
                    HomeAdapter homeAdapter = new HomeAdapter(categories);
                    recyclerView.setAdapter(homeAdapter);
                    recyclerView.setLayoutManager(staggeredGridLayoutManager);
                    homeAdapter.notifyDataSetChanged();
                    homeAdapter.setClickListener(new HomeAdapter.CategoryItemClickListener() {
                        @Override
                        public void onCategoryItemClick(int position) {

                            CategoryModel categoryModel= categories.get(position);
                            activity.selectedCategoryModel = categoryModel;
                            activity.switchFragment();

                        }
                    });

                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });



        return rootView;
    }


}