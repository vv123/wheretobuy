package com.example.wheretobuy.ui.changeItemImage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wheretobuy.R;
import com.example.wheretobuy.Retrofit.ProductModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class ChangeItemImageFragmentAdapter extends RecyclerView.Adapter<ChangeItemImageFragmentAdapter.ChangeItemImageViewHolder>{

    private List<ProductModel> products;
    StorageReference storage = FirebaseStorage.getInstance().getReferenceFromUrl("gs://where-to-buy-293620.appspot.com/");
    private Context context;
    private Activity activity;

    private static final int CAMERA_REQUEST_CODE = 1;


    public ChangeItemImageFragmentAdapter(Context context, List<ProductModel> products) {
        this.products = products;
        this.context = context;
        this.activity = (Activity) context;
    }

    @NonNull
    @Override
    public ChangeItemImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View cardView = inflater.inflate(R.layout.change_item_image_card_view, parent, false);

        // Return a new holder instance
        ChangeItemImageFragmentAdapter.ChangeItemImageViewHolder viewHolder = new  ChangeItemImageFragmentAdapter.ChangeItemImageViewHolder(cardView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ChangeItemImageViewHolder holder, int position) {
        ProductModel product = products.get(position);

        TextView productText = holder.productText;

        productText.setText(product.getTitle());

        final ImageView image = holder.image;

        StorageReference ref = storage.child(product.getImage());

        ref.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downUri = task.getResult();
                     String imageUrl = downUri.toString();
                    Picasso.get()
                            .load(imageUrl)
                            .into(image);
                }
            }
        });


       holder.icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
              /*  File f = new File(Environment.getExternalStorageDirectory(), "POST_IMAGE.jpg");
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                Uri photoURI = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider",f)
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);;*/
                activity.startActivityForResult(intent, CAMERA_REQUEST_CODE);
            }
        });
    }





    @Override
    public int getItemCount() {
        return products.size();
    }

    public class ChangeItemImageViewHolder extends RecyclerView.ViewHolder {

        public TextView productText;
        public ImageView image;
        public Button icon;


        public ChangeItemImageViewHolder(@NonNull View itemView) {
            super(itemView);

            productText = (TextView) itemView.findViewById(R.id.change_item_image_title);
            image = (ImageView) itemView.findViewById(R.id.change_item_image_image);
            icon = (Button) itemView.findViewById(R.id.cameraIcon);


        }
    }
}
