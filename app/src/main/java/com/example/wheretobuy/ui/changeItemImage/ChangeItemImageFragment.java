package com.example.wheretobuy.ui.changeItemImage;

import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.wheretobuy.R;
import com.example.wheretobuy.Retrofit.ProductModel;
import com.example.wheretobuy.ui.wishlist.WishlistAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ChangeItemImageFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<ProductModel> products = new ArrayList<ProductModel>();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final String TAG = "FAILED";




    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.change_item_image_fragment, container, false);
        recyclerView = root.findViewById(R.id.change_image_recycle_view);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        db.collection("Beverage").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<ProductModel> newProducts = new ArrayList<ProductModel>();
                    for (QueryDocumentSnapshot document : task.getResult()) {


                        Object title = document.get("name");
                        Object image = document.get("image");
                        ProductModel pm = new ProductModel();

                        pm.setTitle(title.toString());
                        pm.setImage(image.toString());
                        newProducts.add(pm);
                    }

                    products = newProducts;

                    ChangeItemImageFragmentAdapter changeItemImageFragmentAdapter = new ChangeItemImageFragmentAdapter(getContext(), products);
                    recyclerView.setAdapter(changeItemImageFragmentAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    changeItemImageFragmentAdapter.notifyDataSetChanged();

                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });

        return root;
    }




}