package com.example.wheretobuy.ui.wishlist;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.wheretobuy.R;
import com.example.wheretobuy.Retrofit.CategoryModel;
import com.example.wheretobuy.Retrofit.ProductModel;
import com.example.wheretobuy.ui.home.HomeAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class WishlistFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<ProductModel> products = new ArrayList<ProductModel>();
    FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final String TAG = "FAILED";


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        recyclerView = root.findViewById(R.id.product_recycle_view);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        db.collection("wishlist").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<ProductModel> newProducts = new ArrayList<ProductModel>();
                    for (QueryDocumentSnapshot document : task.getResult()) {


                        Object title = document.get("name");
                        Object id = document.get("id");

                        ProductModel pm = new ProductModel();
                        pm.setId(id.toString());
                        pm.setTitle(title.toString());
                        newProducts.add(pm);
                    }

                    products = newProducts;

                    WishlistAdapter wishlistAdapter = new WishlistAdapter(products);
                    recyclerView.setAdapter(wishlistAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    wishlistAdapter.notifyDataSetChanged();

                } else {
                    Log.w(TAG, "Error getting documents.", task.getException());
                }
            }
        });

        return root;
    }
}